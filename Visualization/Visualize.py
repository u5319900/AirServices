
# coding: utf-8

# In[ ]:

import matplotlib.pyplot as plt
import pandas as pd
import numpy as np
import matplotlib.cm as cm


# In[ ]:

def visualize_cluster(flight_No, cluster_list, k_num, path="Data\odas_ids_full_2.csv", save=False):
    """
    Given flight No, corresponding cluster list and total num of cluster, function draws, first, all flights according
    to their cluster with one colour for each cluster, and second, each cluster into a separate plot.

    Arg:
        flight_No: a list for flight_No to check that the clustered flights are the same as the one read from the file
        cluster_list: a list denoting cluster No for the corresponding flights in flight_No
        k_num: total number of clusters
        path: the path to specify the file to read from
        save=False: if True, the plot result will be saved into the current directory as *.png
     
    Return:
        Null
    """
    # read in data
    data = pd.read_csv(path)
    data = data[["odas_id","lon","lat"]]
    flight_group = data.groupby("odas_id").groups

    #set figure configuration
    plt.figure(figsize=(8,4),dpi=64)
    plt.xlabel('latitude')
    plt.ylabel('longtitude')
    
    #set colour: one colour per cluster
    colours = cm.rainbow(np.linspace(0, 1, k_num))
    
    # all clusters in one plot
    for k,colour_k in zip(range(k_num),colours):
        cluster_k = np.asarray(flight_No)[np.where(cluster_list==k)]
        
        for key in cluster_k:
            data_indexes = flight_group[key] #indexes in dataframe for current flight ID
            flight = (data.loc[data_indexes])[["lon","lat"]].as_matrix() #slice it into matrix
            plt.plot(flight[:,0], flight[:,1], color = colour_k)
    if (save):
        plt.savefig('lon_lat.png',dpi=64)
    plt.show()
    
    # one cluster one plot
    for k,colour_k in zip(range(k_num),colours):
        cluster_k = np.asarray(flight_No)[np.where(cluster_list==k)]
            
        plt.figure(figsize=(8,4),dpi=64)
        plt.xlabel('latitude')
        plt.ylabel('longtitude')
        for key in cluster_k:
            data_indexes = flight_group[key] #indexes in dataframe for current flight ID
            flight = (data.loc[data_indexes])[["lon","lat"]].as_matrix() #slice it into matrix
            plt.plot(flight[:,0], flight[:,1], color = colour_k)
        if (save):
              plt.savefig("cluster_"+str(k)+".png",dpi=64) 
        print(k)
        plt.show()


# In[ ]:

def visualize_height(path="Data\odas_ids_full_2.csv"):
    """
    Plot out the height vs. time plot of all flights inside the specified file.
    
    Arg:
        path: the path to specify the file to read from
     
    Return:
        Null
    """
    # read in data
    data = pd.read_csv(path)
    data = data[["odas_id","lon","lat", "fl"]]
    flight_group = data.groupby("odas_id").groups
    flight_No = data.groupby("odas_id").groups.keys()
    
    #set figure configuration
    plt.figure(figsize = (8,4),dpi=64)
    # all clusters in one plot
    i = 0
    for key in flight_No:
        
        data_indexes = flight_group[key] #indexes in dataframe for current flight ID
        flight = (data.loc[data_indexes])[['fl']].as_matrix() #slice it into matrix
        plt.plot(flight[:,0])

    plt.xlabel('time')
    plt.ylabel('flight altitute')
    plt.show()  


# In[ ]:

def visualize_speed(path="Data\odas_ids_full_2.csv"):
    """
    Plot out the speed vs. time plot of all flights inside the specified file.
    
    Arg:
        path: the path to specify the file to read from
     
    Return:
        Null
    """
    # read in data
    data = pd.read_csv(path)
    flight_group = data.groupby("odas_id").groups
    flight_No = data.groupby("odas_id").groups.keys()
    
    #set figure configuration
    plt.figure(figsize = (4,8),dpi=64)
    # all clusters in one plot
    i = 0
    for key in flight_No:
        
        data_indexes = flight_group[key] #indexes in dataframe for current flight ID
        flight = (data.loc[data_indexes])[['ground_speed']].as_matrix() #slice it into matrix
        plt.plot(flight[:,0])

    plt.axhline(y=100, color='r', linestyle='-')
    plt.axhline(y=200, color='r', linestyle='-')
    plt.axhline(y=300, color='r', linestyle='-')

    plt.xlabel('time')
    plt.ylabel('flight speed')
    plt.show()  


# In[ ]:



