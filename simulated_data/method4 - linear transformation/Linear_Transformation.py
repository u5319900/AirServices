
# coding: utf-8

# In[1]:

import numpy as np


# In[2]:

def distort_route(flight_matrix, distortion):
    """
    Function introduces distortion on a given flight track. The distorted track start at the same point as the original one.
    Note: Original flight matrix will be lost.
    
    Args: 
        flight_matrix: an matrix of ndarray or array_like, which denotes one flight track and should be of shape (-1,7)
        distortion: an matrix of ndarray or array_like, which denotes the distortion and should be of shape (7,-1); It can also be one number,
                    which will then be interpreted into the stretching degree of each flight track.
                    Note: distortion is number & distortion < 0: flipped over according to "start point - end point" line, and then distorted
                          accordingly.

    Return: 
        array_like: a distorted matrix with shape (7,-1), denoting the distorted flight track
    """
    
    start_point = flight_matrix[0]
    distortion = np.matrix(distortion)
    
    if distortion.size == 1:
        distortion_degree = distortion.item(0)
        
        start = flight_matrix[:5,1:3].mean(axis=0)
        end = flight_matrix[-5:,1:3].mean(axis=0)
        direction = end-start
        arc = np.arctan(direction[1]/direction[0])
        
        distortion = np.matrix([[1,0,0,0,0,0,0],
                                [0,np.cos(arc),-np.sin(arc),0,0,0,0],
                                [0,np.sin(arc),np.cos(arc),0,0,0,0],
                                [0,0,0,1,0,0,0],
                                [0,0,0,0,1,0,0],
                                [0,0,0,0,0,1,0],
                                [0,0,0,0,0,0,1]])

        distortion = distortion * np.matrix([[1,0,0,0,0,0,0],
                                             [0,1,0,0,0,0,0],
                                             [0,0,distortion_degree,0,0,0,0],
                                             [0,0,0,1,0,0,0],
                                             [0,0,0,0,1,0,0],
                                             [0,0,0,0,0,1,0],
                                             [0,0,0,0,0,0,1]])

        distortion = distortion * np.matrix([[1,0,0,0,0,0,0],
                                [0,np.cos(-arc),-np.sin(-arc),0,0,0,0],
                                [0,np.sin(-arc),np.cos(-arc),0,0,0,0],
                                [0,0,0,1,0,0,0],
                                [0,0,0,0,1,0,0],
                                [0,0,0,0,0,1,0],
                                [0,0,0,0,0,0,1]])
        
    flight_matrix = flight_matrix@distortion
    flight_matrix -= (flight_matrix[0] - start_point)
    
    return flight_matrix


# In[ ]:



