# -*- coding: utf-8 -*-
import numpy as np
import os
import random
import matplotlib.pyplot as plt
import pandas as pd
import Visualization.Visualize as v
import shutil
from sklearn import mixture
from sklearn import linear_model
from Preprocessing import Data_Preprocess as dp
import matplotlib.cm as cm

#%% set working directory at the root of the project
# os.chdir('C:\\Users\\Ton\\OneDrive\\Document\\Courses\\COMP3100\\Project')
# 
os.getcwd()

#%%
input_data = pd.read_csv('Data/odas_ids_full_2.csv')
data_odas_id_group = input_data.groupby(['odas_id'])
n_flights = np.array(data_odas_id_group).shape[0]
def get_flight_raw(index):
    """
    The function is used to retrieve the corresponding rows of a flight given the index of the flight.

    Args:
        index: The index of the flight we want to get in the DataFrame

    Returns: the corresponding numpy 2d arrays from the input data

    """
    return np.array(data_odas_id_group)[index][1]

def get_flight(index,data):
    grouped = data.groupby(['odas_id'])
    return grouped.get_group(index)
# =============================================================================
# #%%
# # mean lat and lon for each flight
# mean_lat = np.empty(n_flights)
# mean_lon = np.empty(n_flights)
# for i in range(n_flights):
#     mean_lat[i] = np.mean(get_flight_raw(i).loc[:,'lat'])
#     mean_lon[i] = np.mean(get_flight_raw(i).loc[:,'lon'])
# =============================================================================


#%% Adding noise
flight0 = get_flight_raw(59)
flight1 = get_flight_raw(8)
lat = np.array(flight0.loc[:,'lat'])
lon = np.array(flight0.loc[:,'lon'])
plt.plot(lon,lat)
plt.plot(np.array(flight1.loc[:,'lon']),np.array(flight1.loc[:,'lat']))
t = np.arange(0,lat.size)

#%% Piecewise Linear
n_partition = 5
partition_size = int(lat.size/n_partition)
basis = np.empty([lat.size, n_partition + 2])
knots = [0,100,700]
basis[:,0] = t
basis[:,1] = t ** 2
for i in range(n_partition):
    basis[:,i+2] = [(x-i*partition_size)**3 if x>=i*partition_size else 0 for x in t]
# =============================================================================
# for i in range(n_partition):
#      basis[:,i+2] = [(x-knots[i])**3 if x>=knots[i] else 0 for x in t]
# =============================================================================
# Create linear regression object
regr_lat = linear_model.LinearRegression()
regr_lon = linear_model.LinearRegression()

#%%
# Train the model using the training sets
for i in range(10):
    regr_lat.fit(basis, np.reshape(lat+np.random.normal(0,0.6,lat.size),[lat.size,1]))
    regr_lon.fit(basis, np.reshape(lon+np.random.normal(0,0.6,lat.size),[lon.size,1]))
    plt.plot(regr_lon.predict(basis), regr_lat.predict(basis))
plt.plot(lon,lat)
#%%


#n_partition = [5,10,20]
#sds = [0.3,0.5,0.8]


def plot_test_result(flights, partitions, sds, number_of_tracks, 
                     output_csv=False, output_plot = True, name_append = ''):
    """
    The function simulate tracks using cubic splines method.
    It can output the simulated data as csv files and can plot the tracks(latitude vs longitude) and output the png files.
    Args:
        flights: a list of 2d arrays, each correspond to a flight. (use get_flight_raw to retrieve this)
        partitions (int): number of knots we want when fitting the cubic spline
        sds (float): the standard deviation of the normal distribution for generating random noise
        number_of_tracks (int): The number of tracks to simulate around each 'centre'
        output_csv (bool): Whether it is required to output the simulated tracks as a csv file
        output_plot (bool): Whether it is required to plot the simulated tracks and output as png files
        name_append (str): A string to append at the end of the name

    Returns:
        A list of strings consisting the names of csv files created
    """

    csv_names = []
    # Try different partition sizes fo simulating data. 'partitions' contains one element when we fixed the size of knots 
    for n in partitions:
        
        # Try different standard deviation of noises adding to the track
        for sd in sds:
            
            simulated_tracks = pd.DataFrame({'odas_id' : [], 'cluster id' : [], 
                                     'lat' : [], 'lon' : [], 
                                     'time step' : []})
#            simulated_tracks = np.empty([
#                    sum(flight.shape[0] for flight in flights)*number_of_tracks,4])
            flight_id = 0
            
            # generated simulated tracks around each 'flight' from our real dataset
            for index,flight in enumerate(flights):
                
                lat = np.array(flight.loc[:,'lat'])
                lon = np.array(flight.loc[:,'lon'])
                t = np.arange(0,lat.size)
                partition_size = int(lat.size/n)
                basis = np.empty([lat.size, n + 2])
                basis[:,0] = t
                basis[:,1] = t ** 2
                for i in range(n):
                    basis[:,i+2] = [(x-i*partition_size)**3 if x>=i*partition_size else 0 for x in t]
                regr_lat = linear_model.LinearRegression()
                regr_lon = linear_model.LinearRegression()
                plt.plot(lon,lat,color='k')
                for i in range(number_of_tracks):
                    regr_lat.fit(basis, 
                                 np.reshape(lat+np.random.normal(0,sd,lat.size),[lat.size,1]))
                    regr_lon.fit(basis, 
                                 np.reshape(lon+np.random.normal(0,sd,lat.size),[lon.size,1]))
                    
                    
                    simulated_t = np.arange(0,lat.size, np.random.normal(1,0.2))
                    s_basis = np.empty([len(simulated_t), n + 2])
                    s_basis[:,0] = simulated_t
                    s_basis[:,1] = simulated_t ** 2
                    partition_size = int(len(simulated_t)/n)
                    for i in range(n):
                        s_basis[:,i+2] = [(x-i*partition_size)**3 if x>=i*partition_size else 0 for x in simulated_t]

                    simulated = pd.DataFrame({
                            'odas_id' : np.full(len(simulated_t), flight_id), 
                             'cluster id' : np.full(len(simulated_t), index), 
                             'lat' : regr_lat.predict(s_basis).reshape(len(simulated_t)), 
                             'lon' : regr_lon.predict(s_basis).reshape(len(simulated_t)),
                             'time step' : np.arange(len(simulated_t))})
                    simulated_tracks = pd.concat([simulated_tracks, simulated],
                                                ignore_index=True)
                    int_columns = np.array(['odas_id','cluster id','time step'])
                    simulated_tracks[int_columns] = simulated_tracks[
                            int_columns].astype(int)
                    plt.plot(simulated['lon'], simulated['lat'])
                    flight_id += 1
            #print(simulated_tracks)
                
            if output_plot:
                plt.ylabel('latitude')
                plt.xlabel('longitude')
                filename = 'simulated_data/method2/plots/cubic_splines_n'+str(n)+'_sd'+str(sd)+name_append+'.jpg'
                print(filename)
                plt.savefig(filename,dpi=512)
                plt.clf()
    
            #write the simulated data to a CSV file
            if output_csv:
                print('geodesics')
                simulated_tracks.to_csv('simulated_data/method2/CSVs/cubic_splines_n'+
                                        str(n)+'_sd'+str(sd)+name_append+
                                        '.csv',sep=',',header=True,index =False)
                csv_names.append('cubic_splines_n'+
                                        str(n)+'_sd'+str(sd)+name_append+'.csv')
                print(csv_names)
    return csv_names
        
        

# plot_test_result([flight0,flight1],n_partition,sds,6,output_csv=True)

def random_centres_simulation(n, partition, sd, number_of_tracks):
    """
    This function randomly choose n pairs of flights from the true dataset and call the plot_test_result to simulate
    tracks for each pair
    Args:
        n (int): number of pairs to choose
        partition (int): number of knots (for cubic spline method)
        sd (float): standard deviation of the noise (to be passed as the sd parameter of plot_test_result)
        number_of_tracks (int): number of tracks to simulate around each centre

    Returns:
    A list of strings consisting the names of csv files created
    """
    clear_plots()
    csv_names = []
    for i in range(n):
        
        #choose two centres randomly
        a, b = random.sample(set(np.arange(514)), 2)
        csv_names += plot_test_result(
                        [get_flight_raw(a),get_flight_raw(b)],
                        partition,sd, number_of_tracks,
                        output_csv=True, output_plot = True, 
                        name_append='_c'+str(a)+'_'+str(b))
    return csv_names

#%%
def clear_plots():
    dirs=['simulated_data/method2/CSVs','simulated_data/method2/plots']
    for folder in dirs:
        if os.path.isdir(folder):
            shutil.rmtree(folder)
        os.makedirs(folder)
#%%
n_partition = [3,12,48]
sds = [0.5,6,72]
#%%
flight0 = get_flight_raw(0)
flight1 = get_flight_raw(7)
clear_plots()
csv_names = plot_test_result([flight0,flight1],n_partition,sds,number_of_tracks=6,
                 output_csv=True,output_plot=True)
#%%
csv_names = random_centres_simulation(n=1,partition=n_partition,sd=sds,number_of_tracks=10)
#%%
# Create test data
truth = {}
clusters = {0:set(),1:set()}
for csv_name in csv_names:
    simulated_data = pd.read_csv('simulated_data/method2/CSVs/'+csv_name)
    groupby_data = simulated_data.groupby('odas_id')
    testing_data = simulated_data.drop('cluster id', axis=1)
    testing_data.to_csv('simulated_data/method2/CSVs/test_'+csv_name, index = False)
    
    # map flight id to its corresponding cluster id
    ids = simulated_data['odas_id'].unique()
    for id in ids:
        clusters[groupby_data.get_group(id)['cluster id'].iloc[0]].add(id)
        truth[id] = groupby_data.get_group(id)['cluster id'].iloc[0]
        
#%%
path_str = 'simulated_data/method2/CSVs/test_'+csv_names[0]
ideal,avocado = dp.read_in_asmatrix(read_in_method = 'compress',path= path_str)
avocado=dp.feature_normalization(avocado, method='std')

#clustering
n_components = 2
gmm = mixture.GaussianMixture(n_components = n_components)
gmm.fit(avocado)
bonahon = gmm.predict(avocado)
#%%
def visualise(clusters, path, k_num):
    simulated_data = pd.read_csv(path)
    n_flights = simulated_data.groupby(['odas_id']).size().size
    colours = cm.rainbow(np.linspace(0, 1, k_num))
    for i in range(n_flights):
        flight=get_flight(i,simulated_data)
        plt.plot(flight['lon'], flight['lat'],color=colours[clusters[i]])
    plt.savefig('faithful.png')
    plt.show()
    plt.clf()
# visualise(bonahon, path_str, 2)
for csv in csv_names:
    csv_path = 'simulated_data/method2/CSVs/test_'+csv
    visualise(bonahon, csv_path, 2)
        
#%%
n_error = 0
for cluster in clusters:
    if np.mean(bonahon[np.asarray(list(clusters[cluster]))])>0.5:
        n_error += sum(bonahon[np.asarray(list(clusters[cluster]))]==0)
    else:
        n_error += sum(bonahon[np.asarray(list(clusters[cluster]))]==1)
accuracy = 1-n_error/len(bonahon)
print(accuracy)

#%%
n_partition1 = [3,6,12]
sds1 = [0.5,1,2,4,8,16,32]
def eval_performance(n_pairs, cluster_size, sds, n_partition, plot=False):
    csv_namesg = random_centres_simulation(n=n_pairs,partition=n_partition,
                                           sd=sds,
                                           number_of_tracks=cluster_size)
    # Create test data
    accuracy_v_sd = np.empty([0,2],dtype=float)
    for csv_name in csv_namesg:
        truth = {}
        clusters = {0:set(),1:set()}
        simulated_data = pd.read_csv('simulated_data/method2/CSVs/'+csv_name)
        groupby_data = simulated_data.groupby('odas_id')
        testing_data = simulated_data.drop('cluster id', axis=1)
        testing_data.to_csv('simulated_data/method2/CSVs/test_'+csv_name, 
                            index = False)
          
        # map flight id to its corresponding cluster id
        ids = simulated_data['odas_id'].unique()
        for id in ids:
            clusters[groupby_data.get_group(id)['cluster id'].iloc[0]].add(id)
            truth[id] = groupby_data.get_group(id)['cluster id'].iloc[0]
        
        csv_path = 'simulated_data/method2/CSVs/test_'+csv_name
        ideal,avocado = dp.read_in_asmatrix(read_in_method = 'compress',
                                            path= csv_path)
        avocado=dp.feature_normalization(avocado, method='std')

        #clustering
        n_components = 2
        gmm = mixture.GaussianMixture(n_components = n_components)
        gmm.fit(avocado)
        bonahon = gmm.predict(avocado)
        n_error = 0
        for cluster in clusters:
            if np.mean(bonahon[np.asarray(list(clusters[cluster]))])>0.5:
                n_error += sum(bonahon[np.asarray(list(clusters[cluster]))]==0)
            else:
                n_error += sum(bonahon[np.asarray(list(clusters[cluster]))]==1)
        accuracy = 1-n_error/len(bonahon)
        sd = get_deviance(simulated_data, clusters)
        accuracy_v_sd = np.append(accuracy_v_sd, np.array([[accuracy, sd]]), 
                                  axis=0)
    return accuracy_v_sd
eval_performance(1,6, sds1, n_partition1)

#%%
def get_deviance(data,cluster_labels):
    sd = 0
    for cluster in cluster_labels:
        mean_loc=np.empty([len(cluster_labels[cluster]),2])
        for index,flight_id in enumerate(cluster_labels[cluster]):
            mean_loc[index]=[np.mean(get_flight(flight_id,data)['lon']),
                            np.mean(get_flight(flight_id,data)['lat'])]  
        sd+=np.std([np.std(mean_loc[:,1]), np.std(mean_loc[:,0])])
    return sd/len(clusters)
    
    