# -*- coding: utf-8 -*-
"""
Created on Sun Sep 24 09:32:17 2017

@author: EVAN
"""

# -*- coding: utf-8 -*-
"""
Created on Wed Aug 09 12:05:36 2017

@author: EVAN
"""
import numpy as np
import matplotlib.pyplot as plt
import pandas as pd
import random as rand
import sys
sys.path.append("D:\\17s2\\comp3500\\Github\\airservicesanu\\Clustering")
sys.path.append("D:\\17s2\\comp3500\\Github\\airservicesanu\\Preprocessing")
import Data_Preprocess
import K_mean
import math
#%%
PI = 3.1415926

def simplify_data_set(path):
    """
    This function is used to remove unrelevant data from the data set
    This function assumes that all the data list below will not be used in this
    stage.
    
    Args: 
        path: the directory where the input database is stored + the database
name        
    Return: 
        data: a dataframe which stores the simplified data
    """
    data = pd.read_csv(path)
    data = data.drop('fl',axis = 1)
    data = data.drop('ground_speed',axis = 1)
    data = data.drop('track_angle',axis = 1)
    data = data.drop('sum_dist',axis = 1)
    return data

def distort(center_array, k_num, cluster_size,order,fit = 5, mu = 0, sigma = 1.0):
    """
    This function return a dataset that stores the simulated data and save a 
picture that demonstrates the simulated data
    This function assumes that the result of the function run_k_means 
is reliable(may be not perfect)
    
    Args:
        center_array: a matrix that stores the centers of clusters, the clusters
is gained by run_k_mean. one core is stored in a row of the matrix. 
        k_num: the number of clusters we want to get. This number must be same 
as the number of clusters input to run_k_mean.
        cluster_size: the number of tracks in one cluster
        order: a number used for saving the picture of the visualized tracks. 
This number will appear in the name of the picture.
        fit: the order of the polynomial used to fit the simulate data, will be 
input to numpy.polyfit()
        mu: μ of the normal distribution which is used to generate noise
        sigma: σ of the normal distribution which is used to generate noise
        colour: different clusters are shown in different color
    Returns:
        A picture directly saved into memory.
        final_df: a dataframe which stores the simulated data.
        track_id  time     lon    lat    cluster_id
        1         0.00     36.00  151.00 1
        ..        ..       ..     ..     ..
        n         4000.00  35.98  151.02 k
    """
    
    colour = ['b','g','r','c','m','y','k','w']
    
    empty_df = pd.DataFrame(columns = ['odas_id','cluster_no','time','lon','lat'])
    final_df = pd.DataFrame(columns = ['odas_id','cluster_no','time','lon','lat'])    
    cluster = list()  
    for i in range(k_num):
    
       # in order to guarantee the start and end correct
       # select the segment in the middle of the track     
       
       # grub the center and generater noise around the range.
       array_length = int(center_array[i].shape[0]/3)
       time = np.arange(array_length)
       lon =  np.arange(array_length)
       lat =  np.arange(array_length)
       
       time = time.astype(np.float64)
       lon = lon.astype(np.float64)
       lat = lat.astype(np.float64)
       
       cluster.append(empty_df)
       # first loop  - change the matrix to list.
       for j in range (array_length):
           cluster[i] = cluster[i].append({'odas_id':i * cluster_size, 
                               'time': center_array[i][j*3] , 
                               'lon' :center_array[i][j*3 + 1] , 
                               'lat':center_array[i][j*3 + 2],
                               'cluster_no':i},ignore_index = True)     
           time[j] = center_array[i][j*3]
           lon[j] =  center_array[i][j*3 + 1]
           lat[j] =  center_array[i][j*3 + 2] 
       plt.plot(lon,lat,color = 'k' ) 
       
       lat_length = lat.size
       # get the start and stop
       start = 2*int(lat_length/6);
       stop =  4*int(lat_length/6);
       # second loop - generate noise in for the segment        
       for k in range(cluster_size):
                              
           track_time = time
           track_lon = lon
           track_lat = lat
           
           # get the segment
           tmp_time = track_time[start:stop]
           tmp_lat = track_lat[start:stop]
           tmp_lon = track_lon[start:stop]
           #fit the segment  
           func_lon_fit = np.polyfit(tmp_time,tmp_lon,fit )
           func_lat_fit = np.polyfit(tmp_time,tmp_lat,fit )
           #generate function from polytinomial
           func_lon = np.poly1d(func_lon_fit)
           func_lat = np.poly1d(func_lat_fit)
           #generate new segment          
           lat_new = func_lat(tmp_time)
           lon_new = func_lon(tmp_time)
           # add noise to 1..n, skip the first track
           if k > 0 :
               noise = np.random.normal(mu,sigma,lat_new.size)
               #noise = np.sin(lat_new)*0.5
               lat_new = lat_new + noise
               noise = np.random.normal(mu,sigma,lon_new.size)
               #noise = np.cos(lon_new)*0.8
               lon_new = lon_new + noise
           
           #replace the segment
           track_lon[start:stop] = lon_new
           track_lat[start:stop] = lat_new        
           
           #use global function to visualize the data          
           func_lon_fit = np.polyfit(track_time,track_lon,fit )
           func_lat_fit = np.polyfit(track_time,track_lat,fit )
           
           func_lon = np.poly1d(func_lon_fit)
           func_lat = np.poly1d(func_lat_fit)
                     
           track_lon = func_lon(track_time)
           track_lat = func_lat(track_time)
                      
           plt.plot(track_lon,track_lat,color = colour[i])
           #add them to the dataframe
           if k != 0:          
               for r in range(array_length):
                   cluster[i] = cluster[i].append({'odas_id':i * cluster_size + k, 
                                       'cluster_no':i,
                                       'time': track_time[r] , 
                                       'lon' : track_lon[r] , 
                                       'lat':  track_lat[r]},ignore_index = True)
       final_df = final_df.append(cluster[i],ignore_index = False)    
       
    plt.ylabel(u"lat")
    plt.xlabel(u"lon")
    plt.title(u"test data")
    plt.savefig('method3_test_data'+str(k_num)+str(order),dpi = 200)
    plt.show()           
    return final_df 
def select_core(flight_matrix, k_num, deviation):
    '''
    This function is used to select the clusters core for the simulated data. 
Because the old method, i.e. using run_k_mean to select cluster cores, could not 
control the standard deviation between clusters. And if the result's deviation 
is not satisfying, the method which cost a very long time 
will be run again to fetch the new data. 

    This function has no assumption.
    
    Args:
        flight_matrix : the tracks information including time,lon,lat which is 
stored in a matrix.
        k_num : the number of cluster cores we want
        deviation : the standard deviation we expect 
    Return:
        cluster_core : the cluster cores we select
    '''
    # go through all the routes
    # counter for the cluster number
    counter = 0
    #flag to identify the whether the track satisfy the deviation
    flag = True
    loop_len = int(flight_matrix.shape[1]/3)
    
    core = np.empty(shape=(k_num,flight_matrix.shape[1]))

    while counter < k_num :
        
        flag = True # reset the flag
        idx = rand.randint(0,flight_matrix.shape[0]-1)
        if counter == 0:
            core[counter] = flight_matrix[idx]
            counter = counter + 1
            continue
        # check whether the track       
    
        for i in range(counter):
            test_deviation = 0.0;
            for j in range(loop_len):              
                deviation_lon = math.pow(core[i][j*3+1] - flight_matrix[idx,j*3+1],2)
                deviation_lat = math.pow(core[i][j*3+2] - flight_matrix[idx,j*3+2],2)
                test_deviation = test_deviation + math.sqrt(deviation_lon + deviation_lat)

            test_deviation = test_deviation/(1.0 * loop_len)
            #if the deviation is not large enough
            if test_deviation >= deviation:
                flag = False
                break
            
        if flag == True:
            core[counter] = flight_matrix[idx]
            counter = counter + 1;
    return core
#%%        
if __name__ == '__main__':
    
    print ('simplify the data set\n')
    simplify_df = simplify_data_set(path = "D:\\17s2\\comp3500\\Github\\airservicesanu\\Data\\odas_ids_full_2.csv")
    simplify_df.to_csv('method3_simplify_data.csv',sep=',',header=True,index = False)
    
    print ('reading\n')
    flight_no,flight_info = Data_Preprocess.read_in_asmatrix(path = "method3_simplify_data.csv")
    #flight_no,flight_info = read_in_asmatrix(path = "method3_simplify_data.csv")

    for i in range(2,6):
    #could insert a loop here to test
        CLUSTER_NUMBER = i
        if i == 2:
           CLUSTER_SIZE = 25
        elif i == 3:
           CLUSTER_SIZE = 16
        elif i == 4:
           CLUSTER_SIZE = 12
        elif i == 5:
           CLUSTER_SIZE = 10  
        for j in range(1,11):
            print ('selecting\n')
            dev = float(j)/10
            core = select_core(flight_matrix = flight_info, k_num = CLUSTER_NUMBER, deviation = dev)
            #cluster_list,center = K_mean.run_K_mean(CLUSTER_NUMBER,flight_info)
            #cluster_list,center = run_K_mean(CLUSTER_NUMBER,flight_info)
    
            print ('simulating\n')
            final_df = distort(center_array = core,k_num = CLUSTER_NUMBER,cluster_size = CLUSTER_SIZE,fit = 8,
                               order = j,mu = 0, sigma = 1.0)
            output_path = 'method3_test_data_'+str(i)+str(j)+'.csv'
            final_df.to_csv(output_path,sep=',',header=True,index = False)
