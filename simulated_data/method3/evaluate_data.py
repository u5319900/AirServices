# -*- coding: utf-8 -*-
"""
Created on Sun Sep 17 14:32:07 2017

@author: EVAN
"""
#from sklearn import mixture
from sklearn import mixture
import math
import numpy as np
import matplotlib.pyplot as plt
import pandas as pd
import random as rand
import sys
sys.path.append("D:\\17s2\\comp3500\\Github\\airservicesanu\\Clustering")
sys.path.append("D:\\17s2\\comp3500\\Github\\airservicesanu\\Preprocessing")
import Data_Preprocess as Dp
import K_mean
#%%

size = [25,16,12,10]
num = [2,3,4,5]
num_m1 = [3,5]
color = ['b','r','g','y']
'''
the data is stored in the sample 
..\\simulated_data\\method3\\sample\\2-cluster\\name.csv
'''

def separate_data_and_index(path):
    """
    This function is not completed. Need to consider whether we should store
the cluster id in a list or a series
    This function is used to remove the cluster id from the database and 
returns a new dataset without the cluster_id
    This function assumes that the simulated data is reliable(may be not right)
    
    Arg:
        path: a test dataframe
    Return: 
        data: a dataframe without column "cluster_no"
"""
    #read in the test_id and pop the cluster_no
    data = pd.read_csv(path)
    data.pop("cluster_no")
    return data
#input the processed data_set and the assigned cluster number
def test(path,CLUSTER_NUMBER,CLUSTER_SIZE):
    """
    This function is not completed. This function is used to test how the 
simulated data works.
    This function assumes that the tracks that simulated by the distort method 
are reasonable: The mapping between cluster_no and track_id is reasonable
    
    Arg:
        path: a dataframe
    Return:
        accuracy: the test result: the percentage of the right clustered tracks
    """
    matrix_no,matrix = Dp.read_in_asmatrix(path = path)
    
    #matrix = Dp.feature_normalization(matrix, method='min_max')
    #gmm = mixture.GaussianMixture(n_components = CLUSTER_NUMBER)
    #gmm.fit(matrix)
    #cluster_no = gmm.predict(matrix)
    
    cluster_no,core= K_mean.run_K_mean(CLUSTER_NUMBER,matrix = matrix)
    right = 0.00
    strict_right= 0.00
    
    count = np.zeros(CLUSTER_NUMBER)
    flag = np.zeros(CLUSTER_NUMBER)
    
    loop = 0
    #got accuracy
    for i in range(cluster_no.size):
        loop += 1
        tmp = int(cluster_no[i])
        count[tmp] = count[tmp] + 1;    
        flag[tmp] = flag[tmp] + 1;
        #this CLUSTER_SIZE tracks should fall into 1 cluster
        #find which cluster has the most number of tracks
        if loop == CLUSTER_SIZE:
            maxx = 0
            for j in range(CLUSTER_NUMBER):
                if count[maxx] <= count[j]:
                    maxx = j
            #the number of tracks fall into the right cluster
            right = right + count[maxx] 
            count = np.zeros(CLUSTER_NUMBER)
            loop = 0    
    accuracy = right/(CLUSTER_NUMBER*CLUSTER_SIZE)
    for i in range(CLUSTER_NUMBER):
        if flag[i] > CLUSTER_SIZE:            
            strict_right = strict_right + float(CLUSTER_SIZE)
        else:
            strict_right = strict_right + float(flag[i])
    accuracy_strict = strict_right/(CLUSTER_NUMBER*CLUSTER_SIZE)
    #write in a new dataframe 
    return accuracy,accuracy_strict
###############################################################################
#end
###############################################################################
def evaluate(Test_round = 5,Diff_dataset = 2,Diff_cluster = 4,
             prefix = "D:\\17s2\\comp3500\\Github\\airservicesanu\\simulated_data\\method3\\sample\\",
             midfix = "method3_test_data_"):
    '''
    this function is used as a main function to evaluate the simulated data. 
    The parameters evaluated by this function is the cluster number.
    this function has no assumption
    
    Arg:
        Test_round : the number of rounds that will be run
        Diff_dataset : the number of dataset in each cluster
        Diff_cluster : the number of different cluster numbers, in this case  
cluster numbers range from 2 to 5
        prefix: a string used as prefix of the name of input file
        midfix: a string used as a mid part of the name of input file
    Return:
        result: a 2d array that stores the accuracy          
        average: average accuracy of each row in the result array
    Tips: the detail should be modified for different simulated data set
    '''
    result = np.zeros((Diff_cluster,Test_round*Diff_dataset))
    for t in range(Test_round):
       for i in range(Diff_cluster):
           for j in range(Diff_dataset):
               input_path = prefix + str(num[i]) + "-cluster\\"+midfix+str(num[i])+str(j)+".csv"
               #print(input_path)
               #read in the data
               separate_data = separate_data_and_index(input_path)
               separate_data.to_csv('performance_test_data.csv',sep=',',header=True,index = False)
               #performance test data is just a medium outputs
               accuracy,accuracy_strict = test(path = 'performance_test_data.csv',CLUSTER_NUMBER = num[i],CLUSTER_SIZE=size[i])
               result[i][Diff_dataset*t + j] = accuracy
    average = np.mean(result,axis = 1)
    #visualization
    index = np.arange(Diff_cluster)
    
    bar = 0.4 # the width of the bar
    rects = plt.bar(index + bar,average,bar,color = 'b',label = 'accuracy')
    
    plt.xlabel(u"cluster number")
    plt.ylabel(u"accuracy")
    plt.title(u"evaluating")
    
    plt.xticks(index + 3*bar/4,('2','3','4','5'))
    plt.ylim(0,1.2)
    plt.savefig('evaluate result',dpi = 200)
    
    plt.show()
    print(average)
    
def evaluate_m3s(prefix = "D:\\17s2\\comp3500\\Github\\airservicesanu\\simulated_data\\improved-method3\\",
                 midfix = "method3_test_data_",
                 Diff_cluster= 4, Diff_dataset = 10,Test_round = 5):
    '''
    this function is used as a main function to evaluate the simulated data. 
    The parameters evaluated by this function is the standard deviation.
    this function has no assumption
    
    Arg:
        Test_round : the number of rounds that will be run
        Diff_dataset : the number of dataset in each cluster
        Diff_cluster : the number of different cluster numbers, in this case  
cluster numbers range from 2 to 5
        prefix: a string used as prefix of the name of input file
        midfix: a string used as a mid part of the name of input file
    Return:
        result: a 2d array that stores the accuracy          
        average: average accuracy of each row in the result array
    Tips: the detail should be modified for different simulated data set
    '''
    result = np.zeros((Diff_dataset,Test_round))
    result_strict = np.zeros((Diff_dataset,Test_round))
    for i in range(Diff_cluster):    
        for t in range(Test_round):
           for j in range(1,Diff_dataset+1):
               input_path = prefix + str(num[i]) + "-cluster\\"+midfix+str(num[i])+str(j)+".csv"
               #print(input_path)
               #read in the data
               separate_data = separate_data_and_index(input_path)
               separate_data.to_csv('performance_test_data.csv',sep=',',header=True,index = False)
               #performance test data is just a medium outputs
               accuracy,accuracy_strict = test(path = 'performance_test_data.csv',CLUSTER_NUMBER = num[i],CLUSTER_SIZE=size[i])
               result[j-1][t] = accuracy
               result_strict[j-1][t] = accuracy_strict

        average = np.mean(result,axis = 1)
        average_strict = np.mean(result_strict,axis = 1)
        #visualization
        index = np.arange(0.1,(Diff_dataset+1)/10,0.1)
        print(index)
        print(average,average_strict)
        plt.plot(index,average,color = color[i],linestyle = '-',label=str(num[i])+"cluster")
        plt.plot(index,average_strict,color = color[i],linestyle = '--')
        
    plt.ylabel(u"accuracy")
    plt.xlabel(u"deviation")
    plt.title(u"Data evaluation")
    plt.legend(loc = "lower right")
    plt.ylim(0.50,1.10)
    plt.xlim(0.00,1.05)
    plt.savefig('method3_data_evaluation',dpi = 100)
    plt.show()     
    
def evaluate_m1(prefix = "D:\\17s2\\comp3500\\Github\\airservicesanu\\simulated_data\\method1\\",
                 midfix = "method1_test_data",
                 Diff_cluster= 2, Diff_dataset = 4,Test_round = 5):
    '''
    this function is used as a main function to evaluate the simulated data. 
    The parameters evaluated by this function is the standard deviation.
    this function has no assumption
    
    Arg:
        Test_round : the number of rounds that will be run
        Diff_dataset : the number of dataset in each cluster
        Diff_cluster : the number of different cluster numbers, in this case  
cluster numbers range from 2 to 5
        prefix: a string used as prefix of the name of input file
        midfix: a string used as a mid part of the name of input file
    Return:
        result: a 2d array that stores the accuracy          
        average: average accuracy of each row in the result array
    Tips: the detail should be modified for different simulated data set
    '''
    result = np.zeros((Diff_dataset,Test_round))
    result_strict = np.zeros((Diff_dataset,Test_round))
    for i in range(Diff_cluster):    
        for t in range(Test_round):
           for j in range(7,11):
               input_path = prefix + str(num_m1[i]) + "-cluster\\"+midfix+str(num_m1[i])+str(j)+".csv"
               #print(input_path)
               #read in the data
               separate_data = separate_data_and_index(input_path)
               separate_data.to_csv('performance_test_data.csv',sep=',',header=True,index = False)
               #performance test data is just a medium outputs
               accuracy,accuracy_strict = test(path = 'performance_test_data.csv',CLUSTER_NUMBER = num_m1[i],CLUSTER_SIZE=5)
               result[j-7][t] = accuracy
               result_strict[j-7][t] = accuracy_strict

        average = np.mean(result,axis = 1)
        average_strict = np.mean(result_strict,axis = 1)
        #visualization
        index = np.zeros(Diff_dataset)
        a = np.arange(-3,1)
        for order in range(a.size):
            index[order] = math.pow(2,a[order])
        index = -np.sort(-index)
        print(index)
        print(average,average_strict)
        plt.plot(index,average,color = color[i],linestyle = '-',label=str(num[i])+"cluster")
        plt.plot(index,average_strict,color = color[i],linestyle = '--')
    plt.ylabel(u"accuracy")
    plt.xlabel(u"deviation")
    plt.ylim(0.00,1.05)
    plt.title(u"Data evaluation")
    plt.legend(loc = "lower right")
    plt.savefig('method1_data_evaluation',dpi = 100)
    #plt.show()     
if __name__ == '__main__':
    
    evaluate_m3s()
    evaluate_m1()
