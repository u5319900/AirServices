# -*- coding: utf-8 -*-
import numpy as np
import matplotlib.pyplot as plt
import pandas as pd
from scipy.interpolate import interp1d

# Equal 1
def Equal1(lon):
    """
    Function return a value lat.
    A point on the track line could be discribed as (lon,lat), This function is 
used to get lat with a known lon.
    This funciton works with no assumption but the machine could computing 
precisly. The constant is calculated with the first track's 
    start point and end point in th 'odas_ids_full_2.csv'
    
    Arg:
        lon: the known lon of the point
    Return:
        lat: the wantted lat of the point
    """
    lat = 0.5882 * lon - 122.8633
    return lat

#find joint point
def Joint_Point(b):
    """
    Function return a point (lon,lat). Please refer to the description of point
in function Equal1. This point is the intersecting point of vertical line and 
the track line.
    This funciton works with no assumption but the machine could computing 
precisly. The constant is calculated with the first track's 
    start point and end point in th 'odas_ids_full_2.csv'
    
    Arg:
        b : b is the b in the vertical line.
    Return:
        lon:the lon of the intersecting point
        lat:the lat of the intersecting point
    """
    lon = (122.8633 + b) / 2.2882;
    lat = Equal1(lon)
    return lon,lat

#simulated method 1
def simulate_data_1(DS = 7,cluster_num = 3,cluster_size = 5,fit = 8,sigma_x = 0.11,
    sigma_y = 0.187):
    """
    Function return the simulated data set.
    This function assumes that 
    
    Args:
        DS: the index of dx and dy. Used to choose 
        cluster_num: the number of cluster, the true cluster number is 
            2*cluster_num - 1.
        cluster_size : the number of tracks in the one cluster
        color: used to visualized the data, different clusters are shown in
            different color.
        fit : the order of polynomial used to fit the test data
        sigma_x: the scale of normal distribution for lon noise
        sigma_y:  the scale of normal distribution for lat noise
        
        predefined parameters
        dx: standard deviation for lon.The constant is calculated with 
the first track's start point and end point in th 'odas_ids_full_2.csv'.
        dy: standard deviation for lat.The constant is calculated with 
the first track's start point and end point in th 'odas_ids_full_2.csv'
        b_range: The range of b. The constant is calculated with 
the first track's start point and end point in th 'odas_ids_full_2.csv'.
        
        The step length is 1. 
        Please refer to the definition of b in the method documents.
    Returns:
        Picture: A picture to visualize the dataframe.
        DataFrame: A data frame contains test data the structure is like:
            tracks_id  lon  lat   cluster_id
            1          35   149   1
            ..         ..   ..    ..
            2          34.9 148.8 2
            ..         ..   ..    ..
    """
    dx = np.array([4.056161,3.549141,3.042121,2.535101,
                    2.028081,1.521060,1.014040,0.507020,
                    0.253510,0.126755,0.063378,0.031689,
                   0.015844,0.007922,0.003961,0.001981])

    dy = np.array([6.895474,6.033540,5.171605,4.309671,
                    3.447737,2.585803,1.723868,0.861934,
                    0.430967,0.215484,0.107742,0.053871,
                    0.026935,0.013468,0.006734,0.003367])
    '''
    deviation 
    8,7,6,5
    4,3,2,1
    1/2,1/4,1/8,1/16
    1/32,1/64,1/128,1/256
    '''
    colour = ['b','r','g','c','k','m']

    b_range = np.arange(208.59,223.,1)
   
    #define the final data frames
    print('begin')
    final_df = pd.DataFrame(columns = ['odas_id','lon','lat','cluster_no'])
    #generate the structure to store the stage_result
    raw_result = list()
    raw_x_array = np.zeros(shape=((2*cluster_num - 1)*cluster_size,b_range.size))
    raw_y_array = np.zeros(shape=((2*cluster_num - 1)*cluster_size,b_range.size))
    for i in range(0,2*cluster_num - 1):  
        for j in range(0,cluster_size):
            raw_result.append(final_df.append({'odas_id':cluster_size*i+j,
                                              'lon':144.8418,'lat':-37.6674,
                                              'cluster_no':i},ignore_index = True))
        
    #simulate
    for r in range(1,b_range.size - 1):
        index = b_range[r]
        #all center paths is no in the test data set
        point_x,point_y = Joint_Point(index)#get the joint point
        for j1 in range(0,cluster_size):
            tmp_lon = np.random.normal(loc = point_x,scale = sigma_x)
            tmp_lat = np.random.normal(loc = point_y,scale = sigma_y)
            
            raw_x_array[j1][r] = tmp_lon
            raw_y_array[j1][r] = tmp_lat
            
            raw_result[j1] = raw_result[j1].append({'odas_id':j1,
                                       'lon': tmp_lon ,
                                       'lat': tmp_lat,
                                       'cluster_no':0},ignore_index = True)
        #generate the cenctral cluster
        for i in range(1,cluster_num):#build the center point
            point_up_x = point_x - i*dx[DS]
            point_down_x = point_x + i*dx[DS]
            point_up_y = point_y + i*dy[DS]
            point_down_y = point_y - i*dy[DS]
            #generate the central cluster
            #require sort by paths and lon after the simulation
            for j2 in range(0,cluster_size):
                
                #store the random number tmplly
                tmp_lon_up = np.random.normal(loc = point_up_x,scale = sigma_x)
                tmp_lat_up = np.random.normal(loc = point_up_y,scale = sigma_y)
                tmp_lon_down = np.random.normal(loc = point_down_x,scale = sigma_x)
                tmp_lat_down = np.random.normal(loc = point_down_y,scale = sigma_y)
                
                raw_x_array[(i*2-1)*cluster_size + j2][r] = tmp_lon_up
                raw_y_array[(i*2-1)*cluster_size + j2][r] = tmp_lat_up            
                
                raw_result[(i*2-1)*cluster_size + j2] = raw_result[(i*2-1)*cluster_size + j2].append({'odas_id':(i*2-1)*cluster_size + j2,
                                   'lon': tmp_lon_up,
                                   'lat': tmp_lat_up,
                                   'cluster_no':i*2 - 1},ignore_index = True)
                
                raw_x_array[(i*2)*cluster_size + j2][r] = tmp_lon_down
                raw_y_array[(i*2)*cluster_size + j2][r] = tmp_lat_down
                
                raw_result[i*2*cluster_size + j2] = raw_result[i*2*cluster_size + j2].append({'odas_id':(i*2)*cluster_size + j2,
                                   'lon': tmp_lon_down,
                                   'lat': tmp_lat_down,
                                   'cluster_no':i*2},ignore_index = True)
    #sort & merge
    for i in range(0,2*cluster_num - 1):  
        for j in range(0,cluster_size):
            
            raw_x_array[i*cluster_size+j][0] = 144.8418
            raw_y_array[i*cluster_size+j][0] = -37.6674
            raw_x_array[i*cluster_size+j][b_range.size - 1] = 151.1767
            raw_y_array[i*cluster_size+j][b_range.size - 1] = -33.9411
      
            func = np.polyfit(raw_x_array[i*cluster_size+j],raw_y_array[i*cluster_size+j],fit)
            func_show = np.poly1d(func)
            ynew = func_show(raw_x_array[i*cluster_size + j])
            plt.plot(raw_x_array[i*cluster_size+j],ynew,color = colour[i])
            raw_result[i*cluster_size + j] = raw_result[i*cluster_size + j].append({'odas_id':cluster_size*i+j,
                                              'lon':151.1767,'lat':-33.9411,
                                              'cluster_no':i},ignore_index = True)
    
            final_df = final_df.append(raw_result[i*cluster_size + j],ignore_index = True)
    plt.ylabel(u"lat")
    plt.xlabel(u"lon")
    plt.title(u"test data")
    plt.savefig('method1_test_data'+str(cluster_num*2-1)+str(DS),dpi = 100)
    print('done')
    plt.show()

    return final_df
      
#run
if __name__ == '__main__':
    for i in range(2,4):    
        for j in range(7,11):
            final_df = simulate_data_1(DS = j,cluster_num = i)
            final_df.to_csv('method1_test_data' + str(2*i-1)+str(j)+'.csv',sep=',',header=True,index = False)
