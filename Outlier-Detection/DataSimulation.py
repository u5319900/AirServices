# -*- coding: utf-8 -*-
"""
Created on Mon May  1 22:35:14 2017

@author: Dong Luo
"""
%matplotlib inline
import numpy as np
import matplotlib.pyplot as plt
from sklearn.cluster import KMeans
import pandas as pd
import random as rd
from scipy.interpolate import interp1d

#%%
input_data = pd.read_csv('../data/odas_ids_full_2.csv')
##print input_data
##print input_data.iloc[876]['lon']
simulated_starting_lon = input_data.loc[0, 'lon']
simulated_end_lon = input_data.loc[876, 'lon']
simulated_starting_lat = input_data.loc[0, 'lat']
simulated_end_lat = input_data.loc[876, 'lat']
starting_location = np.array([simulated_starting_lon, simulated_starting_lat])
end_location = np.array([simulated_end_lon,simulated_end_lat])
#%%
track1_lon_corner = (3*simulated_starting_lon + simulated_end_lon)/4
track1_lat_corner = (3*simulated_end_lat + simulated_starting_lat)/4
track3_lon_corner = (simulated_starting_lon + 3*simulated_end_lon)/4
track3_lat_corner = (simulated_end_lat + 3*simulated_starting_lat)/4
#%%
straight_num = 800
lon_diff = simulated_end_lon -simulated_starting_lon
lat_diff = simulated_end_lat - simulated_starting_lat

# The distance from lon to 
straight_dist = np.linalg.norm(np.array([lon_diff, lat_diff]))
track1_part1_dist = np.linalg.norm(np.array([lon_diff,3*lat_diff])/4)
track1_part2_dist = np.linalg.norm(np.array([3*lon_diff,lat_diff])/4)

#%%
num_part1 =  track1_part1_dist * straight_num / straight_dist
num_part2 =  track1_part2_dist * straight_num / straight_dist
track1_lon = np.append(np.linspace(simulated_starting_lon, track1_lon_corner,
    np.floor(num_part1)), np.linspace(track1_lon_corner, simulated_end_lon,
    np.floor(num_part2)))
track1_lat = np.append(np.linspace(simulated_starting_lat, track1_lat_corner,
    np.floor(num_part1)), np.linspace(track1_lat_corner, simulated_end_lat,
    np.floor(num_part2)))
track3_lon = np.append(np.linspace(simulated_starting_lon, track3_lon_corner,
    np.floor(num_part2)), np.linspace(track3_lon_corner, simulated_end_lon,
    np.floor(num_part1)))
track3_lat = np.append(np.linspace(simulated_starting_lat, track3_lat_corner,
    np.floor(num_part2)), np.linspace(track3_lat_corner, simulated_end_lat,
    np.floor(num_part1)))
track2_lon = np.linspace(simulated_starting_lon, simulated_end_lon, straight_num)
track2_lat = np.linspace(simulated_starting_lat, simulated_end_lat, straight_num)
#%%
plt.plot(track1_lon,track1_lat)
plt.plot(track2_lon,track2_lat)
plt.plot(track3_lon,track3_lat)
plt.show()
#%%
def create_path(start, corner, end):
    """
    @parameters:
        start, corner, end: three numpy arrays(np.array([lon,lat]))
    return the a sequence of lon and a sequence of lat representing
    a path(line segments) through the corner point
    """
    corner_lon, corner_lat = corner
    start_lon, start_lat = start
    end_lon, end_lat = end
    part1_dist = np.linalg.norm(corner-start)
    part2_dist = np.linalg.norm(end-corner)
    num1 = np.floor(part1_dist * straight_num / straight_dist)
    num2 = np.floor(part2_dist * straight_num / straight_dist)
    lon = np.append(np.linspace(start_lon,corner_lon,num1),
                    np.linspace(corner_lon,end_lon,num1))
    lat = np.append(np.linspace(start_lat,corner_lat,num1),
                    np.linspace(corner_lat,end_lat,num1))
    return lon,lat

def create_corner_groups(corner, sd, size):
    """
    Assume no correlation between lon and lat
    Assume lon and lat have same sd
    """
    corner_lon, corner_lat = corner
    lon = np.random.normal(corner_lon, sd, size)
    lat = np.random.normal(corner_lat, sd, size)
    return lon,lat
#%%
corner1 = np.array([track1_lon_corner, track1_lat_corner])
corner2 = (starting_location + end_location)/2
corner3 = np.array([track3_lon_corner, track3_lat_corner])
group_centres = np.vstack((corner1,corner2,corner3))
#%%
def create_group_tracks(size, sd, corner_mean):
    corners_lon, corners_lat = create_corner_groups(corner_mean, sd, size)
    corners_locations = list()
    for index in range(size):
        corners_locations.append(np.array([corners_lon[index], corners_lat[index]]))
    lons = list()
    lats = list()
    for corner in corners_locations:
        lon, lat = create_path(starting_location, corner, end_location)
        lons.append(lon)
        lats.append(lat)
    return lons, lats
#%%
group_size = 10
group_sd = lon_diff/20
colours = ['C0', 'C1', 'C2', 'C3', 'C4', 'C5', 'C6', 'C7', 'C8', 'C9']
for group_index, corner_centre in enumerate(group_centres):
    lons, lats = create_group_tracks(group_size, group_sd, corner_centre)
    for index in range(group_size):
        plt.plot(lons[index],lats[index],colours[group_index])
plt.savefig('test1',dpi=800)
    
    
