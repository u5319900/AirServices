# -*- coding: utf-8 -*-
"""
Spyder Editor

This is a temporary script file.
"""
import numpy as np
import matplotlib.pyplot as plt
from scipy.interpolate import interp1d
#%%
t = np.arange(0,100)
x1_centre = np.array([40,30,10])
x2_centre = np.array([90,70,60])
y1_centre = np.array([10,30,40])
y2_centre = np.array([60,70,90])
n_groups = 3
sd = np.pi
size = 10
x1 = list()
y1 = list()
x2 = list()
y2 = list()
fixed_centre = [x1_centre,x2_centre, y1_centre, y2_centre]
c_points = [x1, x2, y1, y2]
for i in range(3): 
    for centre in range(4):
        c_points[centre].append(np.random.normal(fixed_centre[centre][i], sd, size))

#%%
def interpolate(centres_x, centres_y):
    x_fixed = np.array([0,centres_x[0],centres_x[1],100])
    y_fixed = np.array([0,centres_y[0],centres_y[1],100])
    t_fixed = np.array([0,30,70,100])
    fx = interp1d(t_fixed, x_fixed, kind = "cubic")
    fy = interp1d(t_fixed, y_fixed, kind = "cubic")
    x = fx(t)
    y = fy(t)
    return x,y
#%%
colours = ['C0','C1','C2']
for group in range(n_groups):
    for i in range(size):
        x,y = interpolate([x1[group][i], x2[group][i]],[y1[group][i], y2[group][i]])
        plt.plot(x,y,colours[group])
plt.savefig('cubic interpolation',dpi=1600)
