#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Wed Apr 26 11:19:55 2017

@author: zefengding
"""
%matplotlib inline
import numpy as np
import matplotlib.pyplot as plt
from sklearn.cluster import KMeans
import pandas as pd
import random
input_data = pd.read_csv('../data/odas_ids_full_2.csv')
##print input_data
##print input_data.iloc[876]['lon']
simulated_starting_lon = input_data.iloc[0]['lon']
simulated_end_lon = input_data.iloc[876]['lon']
simulated_starting_lat = input_data.iloc[0]['lat']
simulated_end_lat = input_data.iloc[876]['lat']
#%%
#print(simulated_end_lat,simulated_end_lon,simulated_starting_lat,simulated_starting_lon)
random_lon1 = random.uniform(simulated_starting_lon,simulated_end_lon)
random_lat1 = random.uniform(simulated_starting_lat,simulated_end_lat)
random_lon2 = random.uniform(simulated_starting_lon,simulated_end_lon)
random_lat2 = random.uniform(simulated_starting_lat,simulated_end_lat)
random_lon3 = random.uniform(simulated_starting_lon,simulated_end_lon)
random_lat3 = random.uniform(simulated_starting_lat,simulated_end_lat)
random_lon4 = random.uniform(simulated_starting_lon,simulated_end_lon)
random_lat4 = random.uniform(simulated_starting_lat,simulated_end_lat)
random_lon5 = random.uniform(simulated_starting_lon,simulated_end_lon)
random_lat5 = random.uniform(simulated_starting_lat,simulated_end_lat)
random_lon6 = random.uniform(simulated_starting_lon,simulated_end_lon)
random_lat6 = random.uniform(simulated_starting_lat,simulated_end_lat)
#%%
x1 = [simulated_starting_lon,random_lon1,simulated_end_lon]
y1 = [simulated_starting_lat,random_lat1,simulated_end_lat]
x2 = [simulated_starting_lon,random_lon2,simulated_end_lon]
y2 = [simulated_starting_lat,random_lat2,simulated_end_lat]
x3 = [simulated_starting_lon,random_lon3,simulated_end_lon]
y3 = [simulated_starting_lat,random_lat3,simulated_end_lat]
x4 = [simulated_starting_lon,random_lon4,simulated_end_lon]
y4 = [simulated_starting_lat,random_lat4,simulated_end_lat]
x5 = [simulated_starting_lon,random_lon5,simulated_end_lon]
y5 = [simulated_starting_lat,random_lat5,simulated_end_lat]
x6 = [simulated_starting_lon,random_lon6,simulated_end_lon]
y6 = [simulated_starting_lat,random_lat6,simulated_end_lat]
#%%
plt.plot(x1,y2,'r')
plt.plot(x2,y2,'g')
plt.plot(x3,y3,'r')
plt.plot(x4,y4,'r')
plt.plot(x5,y5,'r')
plt.plot(x6,y6,'r')
plt.show()
