
# coding: utf-8

# In[1]:


import matplotlib.pyplot as plt
import pandas as pd
import numpy as np
import random as rand
import matplotlib.cm as cm
import time
import sys
sys.path.append("../Preprocessing/")
from Data_Preprocess import *


def general_cost_func(cluster_list, k_num, path = None, normalized_matrix = None):

    """
    Function returns a value to evaluate the performance of a given clustering result. The function believes that center of
    each cluster is the mean of each cluster.
     
    Arg:
        cluster_list(list): a list of prediction of each row in the matrix (returned from "read_in_asmatrix"; flight as row vector)
        k_num(int): the num of total clusters in the cluster_list
        path: if normalized_matrix = None, the path in which the .csv for flight routes is placed
        normalized_matrix: if path=None, normalized_matrix should be the normalized (expand) flight matrix.
     
    Return:
        int: cost as evaluation of the given cluster_list

    """    
    if normalized_matrix is None:
        assert not (path is None)
        normalized_matrix = normalized_matrix = feature_normalization(read_in_asmatrix('expand', path=path)[1])
    cost = 0
    for k in range(k_num):
        index = np.where(cluster_list == k)[0]
        if index.size == 0:
            print("Empty cluster: ", k)
            continue
        center_k = normalized_matrix[index,:].sum(axis=0) / index.size
        dist = (np.array((normalized_matrix[index]-center_k))**2).sum()
        cost += dist
    cost /= cluster_list.size
    return cost


def run_K_mean(k, matrix, label = []):
    """
    Function takes in a matrix, "matrix", (normalized) contaning all the data points and apply K-means given the number of 
    classes, "k". And it prints out the cost function of each iteration. (need to be taken out for speed purpose in the
    future) It will raise exception if keep finding empty class. It returns "cluster_list" which contains the class num for 
    each row in the "matrix"
    If 'label' is given, the cluster labels in the result will use labels from it. It should be a list with lenth equal k
    
    Arg:
        k(int): number of classes
        matrix(np.matrix) : matrix of all the data points and apply K-means given the number of classes, "k".
        label(list): If 'label' is given, the cluster labels in the result will use labels from it.
        
    Return: 
        list: contains the class num for each row in the "matrix"
        list: the list of centers of the clusters
    
    """
    #allocate space
    center = np.empty(shape=(k,matrix.shape[1]))
    cluster_list = np.zeros(shape = matrix.shape[0]) - 1

    #random initialization
    for i in range(k):
        center[i] = matrix[int(rand.random()*matrix.shape[0])]

    empty_class_cnt = 0
    while(True):
        prev_center = center.copy()

        #cluster assignment:
        for i in range(matrix.shape[0]):
            center_dist = float(np.inf)
            cluster_i = -1
            for c in range(center.shape[0]):
                dist = (matrix[i]-center[c])*np.asmatrix(matrix[i]-center[c]).T
                if (dist<center_dist):
                    center_dist = dist
                    cluster_i = c

            cluster_list[i] = cluster_i

        #move center
        for c in range(center.shape[0]):
            index_array = np.where(cluster_list == c)
            vectors_c = matrix[index_array]
            if (vectors_c.size == 0):
                center[c] = matrix[int(rand.random()*matrix.shape[0])]
                empty_class_cnt += 1
            else:
                center[c] = vectors_c.mean(axis=0)

        change_rate =abs(sum(sum(prev_center - center)))
        if (change_rate < 0.01):
            break
        
        if (empty_class_cnt > 10):
            raise RuntimeWarning("Maybe redundant center")

    assert (np.array(cluster_list)>=0).all()

    # replace the label
    if (len(label)>0):
        assert k == len(label)
        for i in range(len(cluster_list)):
            cluster_list[i] = label[int(cluster_list[i])]

    return cluster_list, center


def find_suitable_k(range_k, flight_info = None):
    """
    Function takes a range, "range_k" as input. "flight_info", if provided, needs to be a list corresponding to 
    [flight_No, features_matrix], in which features_matrix needs to be already normalized. Otherwise, it is the normalized 
    matrix returned originally from "read_in_asmatrix('expand')". The function tries different k num one by one, and then 
    calculated a general cost of each result, the general cost is computed on normalized raw data returned from 
    "read_in_asmatrix('expand')" And then it shows a cost-k plot based on result. 
    
    Args:
        range_k (int): The function tries different k num one by one, and then calculated a general cost of each result
        flight_info(list): list corresponding to [flight_No, features_matrix], in which features_matrix needs to be already normalized.
    
    Returns:
        (list, list, list, list, flight_no): list containing all the results from k-mean(cluster_list), center of each cluster (center_list),
        general cost for each clustering result (cost_list), class numbers for each round (k_list), and flight No. corresponding to the class (flight_No)
    """
    
    # used in calculating cost
    [flight_No,flight_matrix] = read_in_asmatrix('expand')
    flight_matrix = feature_normalization(flight_matrix)
    
    if (flight_info == None):
        matrix = flight_matrix
    else:
        assert flight_No == flight_info[0] # make sure sequence of vectors is the same as the one from "read_in_asmatrix"
        [flight_No, matrix] = flight_info
    
    k_list = [x for x in range_k]
    cluster_list = []
    center_list = []
    
    # run k-mean for each k
    start_time = time.time()
    for i in k_list:
        cluster_k, center_k = run_K_mean(i, matrix)
        cluster_list.append(cluster_k)
        center_list.append(center_k)
    end_time = time.time()
    
    # calculate cost for each result
    cost_list = []
    for clustering, k in zip(cluster_list, k_list):
        # calculate cost
        cost = 0
        for i in range(k):
            index_i = np.where(clustering == i)[0]
            if (index_i.size > 0):
                cluster_i = flight_matrix[index_i,:]
                center_i = cluster_i.sum(axis=0)/cluster_i.shape[0]
                dist = (np.array(cluster_i-center_i)**2).sum()
                cost += dist
            else:
                raise RuntimeWarning("Empty clustering!")
        cost /= flight_matrix.shape[0]
        # append to the list
        cost_list.append(cost)
    
    plt.plot(k_list,cost_list)  
    plt.xticks(k_list)
    plt.xlabel('k: num of clusters')
    plt.ylabel('cost')
    plt.show()
    print('time used: ' + str(end_time-start_time))
    
    return (cluster_list, center_list, cost_list, k_list, flight_No)


def run_hierarchy_K_mean(k_list, feature_list):
    """
    This function takes in two parameters:
    'k_list': a list of num which is the number of class you want from each hierarchy
        example: k_list=[2,2,2] will result in 8 cluster at the end and each hierarchy classify sub-data into 2 clusters
    'feature_list': a list of feature matrix for each hierarchy and need too be of the same length as 'k_list'; And matrix 
    inside needs to be of the same row length with each other.
    Function then returns the final result following the hierarchy using given feature matrix for each hierarchy. The final
    cost is also given, evaluated in the same method in 'find_suitable_k'
    
    * Imporvement: can be implemented concurrently, or parallelly
    
    Args:
        k_list(list):  a list of num which is the number of class you want from each hierarchy
        feature_list: 'feature_list': a list of feature matrix for each hierarchy and need too be of the same length as 'k_list'; And matrix inside needs to be of the same row length with each other.
    
    Result:
        list: final result following the hierarchy using given feature matrix for each hierarchy.
        int: final cost 
    """
    
    # calculate total cluster in each hierarchy
    label = []
    cluster_num = 1
    for k in k_list:
        cluster_num *= k
        label.append(range(cluster_num))

    start_t = time.time()
    # for the first hierarchy
    result = np.array(run_K_mean(k_list[0], feature_list[0], label[0]))

    # for each hierarchy (except for the 1st)
    for i in range(1,len(k_list)):
        sub_result = []
        index = []

        # for each cluster from the previous hierarchy
        for k in label[i-1]:
            index.append(np.where(result==k))

            # calculte label for the current hierarchy
            start_label = k*k_list[i]
            end_label = start_label + k_list[i]

            # run K_mean on this cluster
            sub_result.append(np.array(run_K_mean(k=k_list[i], matrix=feature_list[i], label = range(start_label, end_label))))

        # copy sub_result(s) into previous result (update current hierarchy into result)
        for k in label[i-1]:
            result.flat[index[k]] = sub_result[k]
    end_t = time.time()

    # calculate cost
    flight_No, expand_matrix = read_in_asmatrix('expand', None)
    expand_matrix = feature_normalization(expand_matrix)

    cost = 0
    for i in label[-1]:
        index_i = np.where(result == i)[0]
        if (index_i.size > 0):
            center_i = expand_matrix[index_i,:].sum(axis=0) / index_i.size
            dist = (np.array(expand_matrix[index_i,:]-center_i)**2).sum()
            cost += dist
        else:
            raise RuntimeWarning("Empty clustering!")
    cost /= expand_matrix.shape[0]
    print('time used: ' + str(end_t - start_t), '\n', 'cost: '+str(cost))
    return result, cost


# In[ ]:




