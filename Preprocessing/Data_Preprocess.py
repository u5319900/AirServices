
# coding: utf-8

# In[18]:

import matplotlib.pyplot as plt
import pandas as pd
import numpy as np
import random as rand
import matplotlib.cm as cm
import pandas as pd
from sqlalchemy import create_engine
from pathlib import Path
from os import sys


# In[3]:

def compress_info_matrix(matrix, row_num):
    """
    This function fix the row num of a matrix to a certain num denoted as "row_num", by taking out 2 next-to-each-other rows
    and replace them with one row which consists of their element-wise mean value. The row to be replaced is chosen according
    to a guassian distribution with the half of current total row num as mean and variance
     
    Arg:
        matrix: the matrix that describes ONE flight route
        row_num: the target number of rows to compress the 'matrix' to. 
     
    Return:
        mattrix (row_num,): compressed matrix with matrix.shape[0] == row_num
    """
    while(matrix.shape[0] > row_num):
        target = int((matrix.shape[0]/2) + matrix.shape[0]*np.random.randn()) % (matrix.shape[0]-1) # choose target to replace
        substitute = (matrix[target,:]+matrix[target+1,:])/2 #element-wise mean

        matrix[target,:] = substitute
        matrix = np.delete(matrix, (target+1), axis=0)
    matrix[:,0] -= matrix[0,0]
    return matrix


# In[4]:

def expand_info_matrix(matrix, row_num):
    """
    This function fix the row num of a matrix to a certain num denoted as "row_num", by expanding the last row of matrix
     
    Arg:
        matrix: the matrix that describes ONE flight route
        row_num: num of row to expand the matrix to
     
    Return:
        mattrix (row_num,): expanded matrix with matrix.shape[0] == row_num
    """
    while(matrix.shape[0] < row_num):
        matrix = np.insert(matrix, matrix.shape[0], matrix[-1], axis=0)
    matrix[:,0] -= matrix[0,0]
    return matrix


# In[14]:

def read_in_asmatrix(read_in_method = 'compress', basis_functions = None, path = "..\Data\odas_ids_full_2.csv"):
    """
    Function read in the data and preprocess each flight data according to setting in parameter 'read_in_method',
    'basis_functions' is a passed in function to extend more features, which should take in a matrix from row
    data. (matrix for information for one flight, with each row vector as a snapshot for the flight)
    The function returns a pair whose first list is id for each flight (flight No) and the second is corresponding features
    matrix.
    
    Arg:
        read_in_method: string from following choice: 'compress', 'expand' or 'discard', where
            'compress' => call function 'compress_info_matrix' on each flight
            'expand'   => call function 'expand_info_matrix' on each flight
            'discard'  => raw flight data is discarded and in this case 'basis_functions' must not be None
        basis_functions: function to extend more features
        path: a relative path from current position to the .csv dataset
     
    Return:
        tuple: (flight_No, info_matrix), where
            flight_No is a list of each flight No
            info_matrix is a matrix with each flight info in the row corresponding to the flight_No
    """
    if (not (read_in_method in ['compress', 'expand', 'discard'])):
        raise RuntimeError("Invalid 'read_in_method': should be one of 'compress', 'expand' or 'discard'")
    if (read_in_method == 'discard' and basis_functions == None):
        raise RuntimeError("Should at least have one feature")
        
    data = pd.read_csv(path)
    flight_group = data.groupby("odas_id").groups

    # find the minimum/maximum row number of the info matirx for one flight
    #  => column in the final big info_matrix = (minimum/maximum row)*(num of column for features in dataframe) + length of extra features
    if(read_in_method != 'discard'):
        minimum_row = float('inf')
        maximum_row = 0

        for key in flight_group.keys():
            data_indexes = flight_group[key] #indexes in dataframe for current flight ID
            flight = (data.loc[data_indexes]).drop('odas_id', axis=1).as_matrix() #info table for current flight ID
            if (flight.shape[0] < minimum_row):
                minimum_row = flight.shape[0]
            elif (flight.shape[0] > maximum_row):
                maximum_row = flight.shape[0]

        if (read_in_method == 'compress'):
            column_len = minimum_row*(data.shape[1]-1)
        else:
            column_len = maximum_row*(data.shape[1]-1)
    else:
        column_len = 0

    # find length of extra features
    extra_len = 0
    if (basis_functions != None):
        extra_len = len(basis_functions((data.loc[flight_group[list(flight_group.keys())[0]]]).drop('odas_id', axis=1).as_matrix()))

    # row num in the feature matrix
    row_len = len(flight_group.keys()) 
        
    #declare matrix (allocate mem)
    info_matrix = np.empty(shape=[row_len,(column_len + extra_len)])

    # fill the matrix
    for i, key in zip(range(row_len),flight_group.keys()):
        data_indexes = flight_group[key] #indexes in dataframe for current flight ID
        flight = (data.loc[data_indexes]).drop('odas_id', axis=1).as_matrix() #info table for current flight ID
        
        extra_features = []
        if (basis_functions != None):
            extra_features = basis_functions(flight)
        
        if (read_in_method == 'compress'):
            flight = compress_info_matrix(flight, minimum_row)
        elif (read_in_method == 'expand'):
            flight = expand_info_matrix(flight, maximum_row)
        else:
            flight = np.array([])
            
        flight = np.append(flight.flatten(), extra_features)
        info_matrix[i] = flight # add the flatten features into matrix as a row
    
    return (list(flight_group.keys()),np.matrix(info_matrix))


# In[7]:

def feature_normalization(feature_matrix, method = 'std'):
    """
    Function takes in a matrix, assuming each row of this matrix represents an training example,
    and performs different types of normalization based on instruction. The pass-in matrix is intact.

    Arg:
        matrix: the matrix in which each row represents a flight route record
        method = 'std': string of 'std' or 'min_max':
            'std': Matrix is normalized using: (X - mean) / std
                   Note that columns with identical data in "feature_matrix" are deleted.

            'min_max': Matrix is normalized using: (X - min) / (max-min)
            
            Other value will result in RunTimeError
     
    Return:
        matrix: normalized matrix
    """

    matrix = feature_matrix.copy() # don't change the original matrix

    if method == 'std':
        matrix = np.delete(matrix, np.where(matrix.std(axis=0)==0), axis=1) # delete identical column
        
        std_var = matrix.std(axis=0)
        mean = matrix.mean(axis=0)
        matrix = (matrix - mean)/std_var
    elif method == 'min_max':
        matrix = (matrix-matrix.min(axis=0)) / (matrix.max(axis=0)-matrix.min(axis=0))    
    else:
        raise RuntimeError("Arg 'method' only allowed value: 'std', 'min_max'")
    
    return matrix


# In[ ]:

def get_cruising_phase(route, height_threshold=-1, speed_threshold=-1):
    """
    The function return the section in which the flight is thought to be curising.

    Arg:
        route: the matrix of a flight route record, with a row as a snapshot
        height_threshold = -1: the snapshot will be considered only when the height of flight is bigger this threshold
        speed_threshold = -1 : the snapshot will be considered only when the speed of flight is bigger this threshold
        
    Return:
        int: (cruising_start) the index of snapshot in route matrix, which is consider as the start of the cruising phase
        int: (cruising_end) the index of snapshot in route matrix, which is consider as the end of the cruising phase
    """
    height = route[:,3]
    speed = route[:,4]
    
    if height_threshold == -1:
        height_threshold = height.mean()*0.8
    if speed_threshold == -1:
        speed_threshold = speed.mean()*0.8
    
    cruising_start = -1
    cruising_end = -1

    for i in range(height.shape[0]-10):
        start = height[i:i+5].mean()
        end = height[i+5:i+10].mean()

        if (end-start)/5 < 0.1 and height[i]>height_threshold and speed[i]>speed_threshold:
            cruising_start = i
            break

    for i in reversed(range(10, height.shape[0])):
        start = height[i-5:i].mean()
        end = height[i-10:i-5].mean()

        if (end-start)/5 < 0.1 and height[i]>height_threshold and speed[i]>speed_threshold:
            cruising_end = i
            break

    return cruising_start, cruising_end


# In[8]:

def basis_functions(flight):
    """
    This is the currently best features combination to achieve a good result. Function take in an information matrix 
    of one flight and returns a list of features defined by human. 
    Current features contains "total duration", "direction", "mean of deviations from routes"
    
    (The set of features is listed below, from which current features are selected .)
    
    Arg:
        flight: matrix describing one flight with each row as a snapshot
        
    Return:
        list: a list containing features decribed above
    """
    features_list = []

    # features of duration of flight
    total_duration = flight[:,0].max() - flight[:,0].min() # total duration    
    features_list.append(total_duration) # 0
    
    # feature of direction
    start = flight[:5,1:3].mean(axis=0)
    end = flight[-5:,1:3].mean(axis=0)
    direction_vector = end-start
    features_list.append(direction_vector[0]) # 5
    features_list.append(direction_vector[1])
    
    # calculate deviation features
    projection_vector = np.array((-direction_vector[1], direction_vector[0])) # orthogonal vector to direction vector
    deviation = []
    for i in flight[:,1:3]:
        dist_ver = ((i-start)@projection_vector/np.linalg.norm(projection_vector)) # calculate the length on the projection_vector
        deviation.append(dist_ver)
    deviation = np.array(deviation)
    features_list.append(deviation.mean())
    return features_list


#     features_list = []
    
#     cruising_start, cruising_end = get_cruising_phase(flight)
# 
# ​   # features of duration of flight
#     total_duration = flight[:,0].max() - flight[:,0].min() # total duration
#     flying_duration = flight[cruising_end,0] - flight[cruising_start,0] # cruising duration
#     near_ground_duration = total_duration - flying_duration # climbing & landing duration
    
#     features_list.append(total_duration) # 0
#     features_list.append(flying_duration)
#     features_list.append(near_ground_duration)
    
# ​   # features of altitude    
#     features_list.append(flight[:,3].mean()) # average altitude
#     features_list.append(flight[cruising_start:cruising_end, 3].mean()) # average cruising altitude
    
# ​   # feature of direction
#     start = flight[:5,1:3].mean(axis=0)
#     end = flight[-5:,1:3].mean(axis=0)
#     direction_vector = end-start
#     features_list.append(direction_vector[0]) # 5
#     features_list.append(direction_vector[1])
    
# ​   # calculate features measuring how far a plane ever deviated from its direct path
#     projection_vector = np.array((-direction_vector[1], direction_vector[0])) # orthogonal vector to direction vector
# 
#     deviation = []
#     for i in flight[:,1:3]:
#         dist_ver = ((i-start)@projection_vector/np.linalg.norm(projection_vector)) # calculate the length on the projection_vector
#         deviation.append(dist_ver)
#     deviation = np.array(deviation)
    
# ​   # vector denoting Max (>0) deviation
#     features_list.append(deviation.max()) # 7
#     features_list.append(flight[int(np.where(deviation==deviation.max())[0].mean()), 0])
# 
# ​   # vector denoting Min (<0) deviation
#     features_list.append(deviation.min())
#     features_list.append(flight[int(np.where(deviation==deviation.min())[0].mean()), 0])
    
# ​   # vector denoting average Positive/Negative deviation
#     positive_dev_index = np.where(deviation>0)[0]
#     positive_dev = deviation[positive_dev_index]
#     features_list.append(positive_dev.mean()) # 11
#     features_list.append(np.average(flight[positive_dev_index,0], weights=positive_dev))
    
#     negative_dev_index = np.where(deviation<0)[0]
#     negative_dev = deviation[negative_dev_index]
#     features_list.append(negative_dev.mean())
#     features_list.append(np.average(flight[negative_dev_index,0], weights=negative_dev))
    
# ​   # genral deviation info
#     features_list.append(deviation.var())
#     features_list.append(deviation.mean()) # 16
#     return features_list


# In[36]:

def create_db_from_csv(csvFilePath, dbFilePath, chunkSize):
    """
    Method to create sqlite db from csv file 
    
    Arg:
        csvFilePath: File path to input csv file
        dbFilePath: File path to output db file
        chunkSize: chunk size of records while reading from csv
        
    Return:
        list: a list containing features decribed above
    """
    my_file = Path(dbFilePath)
    if my_file.is_file():
        return logErrorAndReturn('Database with filepath ' + dbFilePath + ' already exists')
    try:
        open(csvFilePath, 'r')
    except OSError:
        return logErrorAndReturn('File path for the input csv file is not a valid path ' + csvFilePath)
    print('Reading input csv file from path ' + csvFilePath)
    
    if (chunkSize <= 0):
        return logErrorAndReturn('Chunksize to be fetched should be more than 0')
    print('Reading the first ' + str(chunkSize) + ' records')
    
    print('Printing preview of input file')
    
    print('Read csv file succesfully')
    print('Creating database')
    csv_database = create_engine('sqlite:///' + dbFilePath)
    i = 0
    j = 1
    for df in pd.read_csv(csvFilePath, chunksize=chunkSize, iterator=True):
        df = df.rename(columns={c: c.replace(' ', '') for c in df.columns}) 
        df.index += j
        i+=1
        df.to_sql('table', csv_database, if_exists='append')
        j = df.index[-1] + 1
        print ('.', end='')
    print('Wrote to database successfully')
    return True
                      
def logErrorAndReturn(errorMsg):
    print(errorMsg)
    return False


# In[ ]:



