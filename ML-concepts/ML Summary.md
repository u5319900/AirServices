#### Logistic Regression (Classification)

1. Gradient descent

   - Hypothesis function: $h_\theta(x)=g(x\theta)=\large\frac{1}{1+e^{-x\theta}}$ 

   - Cost function: 

     ​	$\displaystyle J(\theta)=\frac{1}{m}\sum^m_{i=1}[-y^ilog(h_\theta(x^i))-(1-y^i)log(1-h_\theta(x^i))]+\frac{\lambda}{2m}\sum^n_{j=1}\theta_j^2$ 

   - Update rule: $\forall \theta_j \in \theta, \theta_j := \theta_j-\alpha\large\frac{\partial J(\theta)}{\partial\theta_j}$, $\displaystyle \small\frac{\partial J(\theta)}{\partial\theta_j}=\frac{1}{m}\sum^m_{i=1}[(h_\theta(x^i)-y^i)x_j^i] + \frac \lambda m\theta_j$

#### Neural Networks

1. Problem tring to address:

   - Complex non-linear hypotheses
     - hard to train: too many features with their polynomial terms (computer vision)

2. Representation:

   - input layer (layer $1$), hidden layer(s), output layer
     - each layer has $x_0=1$ as bias unit $\small\text{(except for output layer)}$
   - $a_i^j$ = unit $i$ in layer $j$  
   - $\Theta^j$ = matrix of weights controlling function mapping from layer $j$ to layer $j+1$ 
     - with $s_j$units in layer $j$ and $s_{j+1}$ units in layer $j+1$, $\Theta^j$ is of dimension $s_{j+1}\times(s_j+1)$ 

3. Forward propagation:

   - $h_\Theta( \Theta_j \cdot [a_0^j,a_1^j,...,a_{s_j}^j]^T )=[a_1^{j+1},a_2^{j+1},...,a_{s_{j+1}}^{j+1}]^T$ 

4. **Backward propagation**:

   - $\displaystyle J(\Theta)=-\frac{1}{m}\sum^m_{i=1}\sum^K_{k=1}[ y_k^i log((h_\Theta(x^i))_k) + (1-y_k^i)log(1-(h_\Theta(x^i))_k)] + \frac{\lambda}{2m}\sum_{l=1}^{L-1}\sum_{i=1}^{s_{l+1}}\sum_{j=1}^{s_l}(\Theta_{i,j}^l)^2$ 

     - sum the cost for all k categories
     - $s_l$: num of units in $l$th layer; $\Theta^l$: $s_{l+1}\times(s_i+1)$ => in $\Theta_{i,j},\space i\neq0$ 

   - $\displaystyle \frac{\partial J(\Theta)}{\partial \Theta_{i,j}^l} = a_j^l\delta_i^{l+1}$ 

     - $\delta^L=a^L-y$, $L$ is the num of layers => $a^L$: output layer

     - $\forall l<L,\space \delta^l=(\Theta^l)^T\delta^{l+1}.*g'(z^l)$ , where $g'(z^l) = a^l.*(1-a^l)$ 

       ​	Formally, $\delta_j^l=\frac{\partial J(\Theta)}{\partial z_j^l}$ 

       ​	**// why $\color{red} \delta^l=(\Theta^l)^T\delta^{l+1}.*g'(z^l)$ ?** 

   - **Algorithm**:

     - $\forall l,i,j,\space \Delta^l_{ij}:=0$ 

     - For $i=1$ to $m$ 

       ​	$a^1:=x^i$,  compute $a^l,\space l=2,...,L$ (forward)

       ​	$\delta^L:=a^L-y^i$, compute $\delta^{L-1},...,\delta^2$ (backward,  $a,y^i$ are column vectors )

       ​	$\Delta^l_{ij} += a_j^l\delta_i^{l+1}$ (vectorization: $\Delta^l+=\delta^{l+1}(a^l)^T$)

       ​	$\begin{equation} D_{i,j}^l=   \begin{cases} \frac1m(\Delta_{i,j}^l + \lambda\Theta_{i,j}^l) & j\neq0, \\  \frac1m(\Delta_{i,j}^l & j=0. \end{cases}  \end{equation}$ 

     - $D_{i,j}^l=\frac{\partial J(\Theta)}{\partial \Theta^l_{i,j}}$ 

5. Gradient checking: $D_{i,j} \approx \frac{J(\Theta_{i,j}+\epsilon) - J(\Theta_{i,j}-\epsilon)}{2\epsilon}$ 

   - turn checking off when running the backpropagation algorithm

6. Random initialization: 

   - Symmetry problem:![Symmetry Problem .png](F:\Coursera\Machine Learning\Symmetry Problem .png)

     - so that units in the same layer are computing the same features

   - Symmetry breaking: $\Theta_{i,j}^l := random[-\epsilon, \epsilon]$

     - Default choice: $\displaystyle \epsilon=\frac{\sqrt{6}}{\sqrt{s_l + s_{l+1}}}$, $s_l$ is num of units in layer $l$

     - Matlab/Octave: $\Theta = rand(m, n) * 2\epsilon - \epsilon$

       ​	$rand(m, n)$ => m\*n matrix with randome num between 0,1

7. Network architecture

   - Num of input units: dimension of eatures in $x^i$
   - Num of output units: num of classes (in Classification)
   - Num of hidden layer:
     - default: 1
     - if >1: same num of units in each hidden layer
   - Num of units in layer: comparable to num of layer

#### Support Vector Machine (SVM)

- Do perform feature scaling **before** using Guassian Kernel

1. Hypothesis function: $\begin{equation} h_\theta(x)= \begin{cases} 1 & \theta^Tx \geq 0, \\ 0 & \small \text{otherwise.} \end{cases}  \end{equation}$

2. Cost Function:

   - $\displaystyle J(\theta)=C \sum^m_{i=1}[y^icost_1(\theta^Tx^i)+(1-y^i)cost_0(\theta^Tx^i)]+\frac12 \sum^n_{i=1}\theta^2_j$  

3. SVM Decision Boundary (Large margin classification)

   - $\displaystyle min_\theta \frac12 \sum_{j=1}^n \theta_j^2 = min_\theta\frac12||\theta||^2$ 
     - vector $\theta$ vertical to the decision boundary
     - samll margin => small projection to $\theta$  => large $\theta$ $\small \text{due to $h_\theta(x)$}$ => contradiction         **margin is actually the projection from data to $\theta$** 

4. **Kernel**:

   - Similarity function (a way to measure distance between examples):
     - $\displaystyle f_i = similarity(x,l^i) = e^{-\frac{||x_j - l_j^i||^2}{2\sigma^2}}$ (Guassian Kernel)
   - Hypothesis function: $\begin{equation} h_\theta(x) = \begin{cases} 1 & \theta^Tf \geq 0, \\ 0 & \small \text{otherwise.} \end{cases} \end{equation}$
     - $f = [f_0, f_1, f_2,...,f_n], \space \small f_i = similarity(x,l^i), f_0=1$
   - Choosing landmark $l$:
     - Feature scaling
     - Initialize $m$ landmarks to training examples
     - map $x$ -> $f$, $\small \text{with } f_0 = 1$ 
     - Traning SVM: solving minimization poblem of $J(\theta)$ using $(f,y)$ instead of $(x,y)$)

5. SVM parameters:

   - $C ( = \frac1\lambda)$:
     - Large $C$: lower bias, higher variance (small $\lambda$)
     - Small $C$: higher bias, lower variance (large $\lambda$)
   - $\sigma^2 \small \text{ in Guassian Kernel}$:
     - Large $\sigma^2$: features $f_i$ vary more smoothly, higher bias, lower variance
     - Small $\sigma^2$: features $f_i$ vary less smoothly, lower bias, higher variance
   - Similarity function (Kernel): 
     - Linear kernel (no kernel): $f = x$ 
     - Guassian kernel
     - Polynomial kernel: $f = similarity(x, l) = (x^Tl + a)^b, a,b \in N, x,l \text{ $\small\text{(usually)}$} \in R^+$ 
     - String kernel, Chi-square kernel, Histogram kernel, Intersection kernel...

6. Comparison between logistic regression: $n$ - num of features, $m$ - num of traning data

   - $n >> m$ $\small \text{($n\geq10m$)}$: Logistic regression or SVM with Linear kernel

     $\small \text{(large n => linear function is enough & small m => not enough data to train complicated hypothesis function)}$

   - small $n$ with intermidiate $m$ ($m \approx 10n$): SVM with Guassian kernel

   - $n << m$ ($\small \text{$10n \leq m$}$): **add more features** and then Logistic regression or SVM with Linear kernel

   **(Logistic regression and SVM with Linear kernel are similar)** 

#### Clustering - K-means

1. Input:

   - $K$ (num of clusters)
   - Training set {$x^1,x^2,...,x^m$}, $x^i \in R^n$ (drop $x_0=1$ convention), $K<m$

2. Cost function:

   - $\displaystyle J(c^1,...,c^m,\mu_1,...,\mu_K) = \frac1m \sum^m_{i=1} ||x^i-\mu_{c^i}||^2$ 
     - $c^i= \text{index of cluster {1,2,...,K} assigned to } x^i \text{, K = num of cluster}$ 
     - $\mu_k = \text{cluster centroid }k \text{, $\mu_k \in R^n$}$ 
     - $\displaystyle \mu_{c^i} = \text{cluster centroid assigned to $x^i$}$

3. Algorithm:

   - Initialize K cluster centroid $\mu_1, \mu_2,...,\mu_K \in R^n$ 

   - Repeat

     - for $i=1$ to $m$ $\small \text{(cluster assignment)}$ 

       ​	$c^i := \text{closest cluster centroid to } x^i$ 

     - for $k=1$ to $K$ $\small \text{(move centroid)}$ 

       ​	$\mu_k := \text{mean of points assigned to ckuster } k$ 

4. Initialization:

   - **Randomly** pick $K$ **distinct** examples and set $\mu_1,...,\mu_K$ to them 
     - random algorithm may need to repeat multiple times to avoid local optimum

5. Understanding:

   - Cluster assignment: adjust $c^1,...,c^m$ to minimize $J$, holding $\mu_1,...,\mu_K$ 
   - Move centroid: adjust $\mu_1,...\mu_K$ to minimize $J$, holding $c^1,...,c^m$ 
   - $J$ is **not** possible to increase if algorithm is implemented correctly

6. Parameter $K$: 

   - Elbow method: plot $J-K$ graph:

     ![Elbow method](F:\Coursera\Machine Learning\Elbow method.png)  

   - Consider downstream purpose

#### Anomaly Detection

1. Problem to solve:

   - Given dataset {$x^1,x^2,...,x^m$}, build density estimation model $p(x)$
   - $p(x^{test}<\epsilon)$ => $x^{test}$ anomaly 

2. Hypothesis function: 

   - $\displaystyle p(x)=\prod^n_{i=1} p(x_i), \space x \in R^n,\forall i \in [1,n], \space x_i \sim N(\mu_i,\sigma_i^2)$ 
     - $\displaystyle \mu=\frac1m \sum^m_{i=1}x^i,\space \sigma^2=\frac1m \sum^m_{i=1}(x^i-\mu)^2$ 
     - assume $x_1,...,x_n$ independent from each other

3. Multivariate Gaussian:

   - $\displaystyle p(x;\mu,\Sigma)=\frac1{(2\pi)^{\frac n2} |\Sigma|^{\frac 12}} exp(-\frac12 (x-\mu)^T \Sigma^{-1} (x-\mu)),$  

     $x\in R^n, \mu\in R^n,\Sigma\in R^{n\times n}$, where $\Sigma$ is covariance matrix

     - $\displaystyle \mu=\frac1m \sum^m_{i=1}x^i,\space \Sigma=\frac 1m \sum^m_{i=1}(x^i-\mu)(x^i-\mu)^T$ 
     - $x_1,...x_n$ can be correlated but **not** linearly dependent
     - need $m > n$ $(m\ge10n\space suggested)$ or elas $\Sigma$ non-invertible

4. Algorithm:

   - choose features
   - compute $\mu$, $\sigma$
   - compute $p(x)$ for new example, anomaly if $p(x) < \epsilon $ 

5. Evaluation (real-number):

   - Labeled data into normal/anomalous set

     (okay if some anomalies slip into normal set)

     - training set: unlabeled data from normal set (60%)
     - CV set: labeled data from normal (20%) & anomalous (50%) set
     - test set: labeled data from normal (20%) & anomalous (50%) set

   - Use evaluation metrics (skewed data)

6. When to use:

   - Anomaly detection:
     - Very small num of positive data (0-20 commonly); Large num of negative data
     - Difficult to learn from positive data (not enough data, too many features...)
     - Future anomalies may look nothing like given data
   - Supervised Learning:
     - Larger num of positive & negative data
     - Enough positive data for algorithm to learn
     - Future positive example is likely to be similar to given data

7. Example:

   - Anomaly detection:
   - Fraud detection, Manufacturing, Monitoring machines in data center...
   - Supervised learning:
     - Email spam classification (enough data), Weather prediction (sunny/rainy/etc), Cancer classification...

8. Tips:

   - Non-guassian feature: transformation / using other distribution
   - Choosing features: compare anomaly data with normal data