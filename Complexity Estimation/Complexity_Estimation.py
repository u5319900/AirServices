
# coding: utf-8

# In[1]:

import numpy as np
from sklearn.discriminant_analysis import LinearDiscriminantAnalysis

def estimate_complexity(raw_norm_flights, cluster_list, k_num):
    """
    This function apply Fisher's Linear Discriminant to complexity estimation. It takes the clustering result as ground truth,
    the flight matrix as data point, and then finds the optimal Fisher's Linear Discriminant. At last, it use the reciprocal
    of optimzation crtieria in Fisher's Linear Discriminant as the estimated complexity.
    
    * improvement: 
        1. if weighted each snapshot in a flight route corresponding to time => more focus on most recent flight history
        2. the function now is quite sensitive to the complexity and overlapping between clusteres and the order of magnitude
           of result can change greatly. (In the example, difference between good and bad is 5 magnitude)
           => can consider to take a log or exp on the result to smoothen it.
    Arg:
        raw_norm_flights: ndarray, where each row is a flight route
        cluster_list: a list of clustering result, corresponding to each row in 'raw_norm_flights'
        k_num: the number of clusters (number of classes to be classified)
     
    Return:
        float: a number describing the estimated complexity of the given flight matrix. 
               Larger the number is, more complex the given flights are
    """

    S_w = 0
    S_b = 0
    mean = raw_norm_flights.mean(axis=0)
    for k in range(k_num):
        X_k = raw_norm_flights[np.where(cluster_list == k)]
        mean_k = X_k.mean(axis=0)
        S_w += np.matrix(X_k-mean_k).T @ np.matrix(X_k-mean_k)
        S_b += np.matrix(mean_k-mean).T @ np.matrix(mean_k-mean) * X_k.shape[0]
    S_b /= raw_norm_flights.shape[0]
    
    lda = LinearDiscriminantAnalysis(n_components=k_num)
    lda.fit(raw_norm_flights, cluster_list)
    
    return np.trace(lda.coef_@S_w@lda.coef_.T) / np.trace(lda.coef_@S_b@lda.coef_.T)


# In[ ]:



